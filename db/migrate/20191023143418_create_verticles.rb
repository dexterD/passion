class CreateVerticles < ActiveRecord::Migration[6.0]
  def change
    create_table :verticles do |t|
      t.string :name

      t.timestamps
    end
  end
end
