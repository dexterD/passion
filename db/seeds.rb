class Category
  skip_callback :create, :after, :send_email
end
class Course
  skip_callback :create, :after, :send_email
end

verticles_json = JSON.parse(File.read('db/verticles.json'))
verticles_json.each do |verticle|
  Verticle.create(name: verticle['Name'])
end

categories_json = JSON.parse(File.read('db/categories.json'))
categories_json.each do |category|
  Category.create(name: category['Name'], state: true, verticle_id: category['Verticals'])
end

courses_json = JSON.parse(File.read('db/courses.json'))
courses_json.each do |course|
  Course.create(name: course['Name'], state: true, category_id: course['Categories'], author: course['Author'])
end
