Rails.application.routes.draw do
  resources :courses
  resources :categories do
    get :courses, on: :member
  end
  resources :verticles do
    get :categories, on: :member
  end
  root to: 'home#index'

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks'}
end
