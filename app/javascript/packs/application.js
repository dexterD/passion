
require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")
import "bootstrap"
import "../stylesheets/application"
$('[data-toggle="tooltip"]').tooltip()
$('[data-toggle="popover"]').popover()

