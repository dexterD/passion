import React from "react";
import $ from "jquery";

class Collapse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      parents: [],
      childrens: [],
      grandchildrens: []
    };
  }

  componentDidMount() {
    $.getJSON("/verticles.json", response => {
      this.setState({ parents: response });
    });
  }

  handleClick(id) {
    $.getJSON(`/verticles/${id}/categories.json`, response => {
      this.setState({ childrens: response, grandchildrens: [] });
    });
  }

  handleClickChildren(id) {
    $.getJSON(`/categories/${id}/courses.json`, response => {
      this.setState({ grandchildrens: response });
    });
  }

  renderAccordionCollapse(items) {
    const { childrens } = this.state;
    return (
      <div class="accordion container" id="accordionOne">
        {items.map(item => {
          return (
            <div class="card">
              <div class="card-header" id={`heading_parent__${item.id}`}>
                <h5 class="mb-0">
                  <button
                    class="btn btn-link"
                    onClick={this.handleClick.bind(this, item.id)}
                    data-toggle="collapse"
                    data-id={item.id}
                    data-target={`#collapse_parent__${item.id}`}
                    aria-expanded="false"
                    aria-controls={`#collapse_parent__${item.id}`}
                  >
                    {item.id}. {item.name}
                  </button>
                </h5>
              </div>
              <div
                id={`collapse_parent__${item.id}`}
                class="collapse"
                aria-labelledby={`heading_parent__${item.id}`}
                data-parent="#accordionOne"
              >
                <div class="card-body">
                  <div>
                    {
                      <div class="accordion" id={`accordionTwo__${item.id}`}>
                        <div>
                          <h3>Categories For {item.name}</h3>
                        </div>

                        {childrens.length >= 1 ? (
                          childrens.map(it => {
                            return this.renderChild(it, item);
                          })
                        ) : (
                          <div>
                            <p>No Records Found</p>
                          </div>
                        )}
                      </div>
                    }
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }

  renderGrandchildren(item) {
    return (
      <li class="list-group-item list-group-item-secondary">
        Id: {item.id}, Name: {item.name}, Author: {item.author}
      </li>
    );
  }

  renderChild(item, parent) {
    return (
      <div class="card">
        <div class="card-header" id={`heading_${parent.id}_child__${item.id}`}>
          <h5 class="mb-0">
            <button
              class="btn btn-link"
              onClick={this.handleClickChildren.bind(this, item.id)}
              data-toggle="collapse"
              data-id={item.id}
              data-target={`#collapse_${parent.id}_child__${item.id}`}
              aria-expanded="false"
              aria-controls={`#collapse_${parent.id}_child__${item.id}`}
            >
              {item.id}. {item.name}
            </button>
          </h5>
        </div>
        <div
          id={`collapse_${parent.id}_child__${item.id}`}
          class="collapse"
          aria-labelledby={`heading_${parent.id}_child__${item.id}`}
          data-parent={`#accordionTwo__${parent.id}`}
        >
          <div class="card-body">
            <div>
              <div>
                <h4>Courses For {item.name}</h4>
              </div>
              <ul class="list-group ">
                {this.state.grandchildrens.map(it => {
                  return this.renderGrandchildren(it);
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return <div>{this.renderAccordionCollapse(this.state.parents)}</div>;
  }
}

export default Collapse;
