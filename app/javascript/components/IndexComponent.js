import React from 'react'
import $ from 'jquery'

class IndexComponent extends React.Component{
  constructor(props) {
    super(props);
    this.state = {date: new Date(),
      items: [] 
    };
  }

  componentDidMount() {
    $.getJSON(document.URL+'.json', (response) => { this.setState({ items: response }) });
  }
  
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
        {
          this.state.items.map((item) =>{
            return(
              <div key={item.id}>
                <h3>{item.name}</h3>
                <p>{item.url}</p>
              </div>
            )
          })
        }
      </div>
    );
  }


}

export default IndexComponent