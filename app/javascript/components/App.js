import React from 'react'
import {GoogleAPI, GoogleLogin, GoogleLogout} from 'react-google-oauth';
import Cookies from 'universal-cookie';

class App extends React.Component{
  render(){
    return(
      <div> 
       <GoogleAPI className="GoogleLogin" clientId="487000830714-8sl9aqij271npnghm74mscvj49mhp48s.apps.googleusercontent.com">
          <div>
            <GoogleLogin height="10" width="500px" backgroundColor="#4285f4" access="offline" scope="email profile" onLoginSuccess={this.responseGoogle} onFailure={this.responseGoogle}/>
          </div>
        </GoogleAPI>
      </div>
    )
  }

  responseGoogle(response){
    var token = response.Zi;
    const csrf = document.querySelector("meta[name='csrf-token']").getAttribute("content");
    const requestOptions = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${response.Zi.accessToken}`,
      'Content-Type': 'application/json',
      'access_token': `${response.Zi.accessToken}`,
      'X-CSRF-Token': csrf,
      },
      body: JSON.stringify(token)
    }

    fetch('/auth/google_oauth2/callback', requestOptions)
    .then(r=> r.json())
    .then(resp => {
      console.log(resp)
      const cookies = new Cookies();
      // var date = new Date();
      // var expiry = date.setTime(date.getTime() + (60*60*1000));
      cookies.set('accesstoken', resp.tokens['access-token']);
      cookies.set('client',resp.tokens['client']);
      cookies.set('tokentype',resp.tokens['token-type']);
      cookies.set('expiry',resp.tokens['expiry']);
      cookies.set('uid', resp.tokens['uid']);
      location.href = "/verticles/1/categories"
    })
    // resp.redirect('/verticles/1/categories')
    // console.log(response)
  }
  
}

export default App