class VerticlesController < ApplicationController
  before_action :set_verticle, only: [:show, :edit, :update, :destroy, :categories]
  before_action :authenticate_user!
  # GET /verticles
  # GET /verticles.json
  def index
    @verticles = Verticle.includes(:categories).all
  end

  # GET /verticles/1
  # GET /verticles/1.json
  def show
  end

  # GET /verticles/new
  def new
    @verticle = Verticle.new
  end

  # GET /verticles/1/edit
  def edit
  end

  # GET /verticles/1/categories.json
  def categories
    @categories = @verticle.categories.paginate(page: params[:page], per_page: 10)
  end

  # POST /verticles
  # POST /verticles.json
  def create
    @verticle = Verticle.new(verticle_params)

    respond_to do |format|
      if @verticle.save
        format.html { redirect_to @verticle, notice: 'Verticle was successfully created.' }
        format.json { render :show, status: :created, location: @verticle }
      else
        format.html { render :new }
        format.json { render json: @verticle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /verticles/1
  # PATCH/PUT /verticles/1.json
  def update
    respond_to do |format|
      if @verticle.update(verticle_params)
        format.html { redirect_to @verticle, notice: 'Verticle was successfully updated.' }
        format.json { render :show, status: :ok, location: @verticle }
      else
        format.html { render :edit }
        format.json { render json: @verticle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /verticles/1
  # DELETE /verticles/1.json
  def destroy
    @verticle.destroy
    respond_to do |format|
      format.html { redirect_to verticles_url, notice: 'Verticle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_verticle
      @verticle = Verticle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def verticle_params
      params.require(:verticle).permit(:name)
    end
end
