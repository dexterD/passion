class UserMailer < ApplicationMailer
  default from: 'notifications@passion.com'

  def new_record
    mail(to: 'test@gmail.com', subject: 'New Record Created')
  end
  
end
