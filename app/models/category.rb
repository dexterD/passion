class Category < ApplicationRecord
  belongs_to :verticle
  has_many :courses
  validates_uniqueness_of :name
  after_create :send_email
  private
  def send_email
    UserMailer.new_record.deliver_now
  end
end
