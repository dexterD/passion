class Course < ApplicationRecord
  belongs_to :category
  validates_uniqueness_of :name
  after_create :send_email
  private
  def send_email
    UserMailer.new_record.deliver_now
  end
end
