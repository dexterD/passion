json.extract! category, :id, :name, :state, :verticle_id
json.url category_url(category, format: :json)
