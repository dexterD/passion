json.extract! verticle, :id, :name, :created_at, :updated_at
json.url verticle_url(verticle, format: :json)
