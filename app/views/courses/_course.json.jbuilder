json.extract! course, :id, :name, :state, :author, :category_id
json.url course_url(course, format: :json)
