# README

* Ruby version: 2.6.3

* System dependencies: postgres yarn 

* Database creation rake db:create

* Database initialization rake db:migrate
                          rake db:seed

* How to run the test suite: rake test


As you can see, categories have a parent called vertical and courses have a parent called categories.



# Tasks Completed
  1. To build an UI where you can fold / unfold based on the lists with ReactJS. 

  2. Use the API oauth provider to identify your self

  3. Provide Seed File
  
  4. Validation Of Uniquness of name(Done in 2 models Categories & Courses)
  
  5. Send Email on Create(Done in 2 models Categories & Courses)
  
  6. Added Pagination

# How does your solution perform & scale?
  From the API side, the solution is pretty well designed. We can make also use of pagination.  



# What would you improve next?

  1. Currently I have only integrated one list per model, which can be improved
  
  2. The react code can be improved using reusable components which are missing
  
  3. Remove the secret key and api key from devise, Its easier to test I feel if I keep, thats the reason for keeping it. I could have used ENV variable also. Better practise is to always keep in ENV varibles.
  
  4. Testing for authorization is not proper.There are no proper helpers which I can use in test in the version of rails for this project
  
  5. Css for pages can be better

# What was one of the biggest coding challenges that you ever had?

  In this project I faced problem for integration of google oauth.
  Legacy People api, gave me trouble integrating https://i.imgur.com/2T0xt3l.png.  So I choose to use facebook Oauth. For the biggest coding challenge I ever had will keep for next phase 
