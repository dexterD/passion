require 'test_helper'

class VerticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @verticle = verticles(:one)
    @user = users(:one)
  end

  test "should get index" do
    get verticles_url
    assert_response :success
  end

  test "should get new" do
    get new_verticle_url
    assert_response :success
  end

  test "should create verticle" do
    assert_difference('Verticle.count') do
      post verticles_url, params: { verticle: { name: @verticle.name } }
    end

    assert_redirected_to verticle_url(Verticle.last)
  end

  test "should show verticle" do
    get verticle_url(@verticle)
    assert_response :success
  end

  test "should get edit" do
    get edit_verticle_url(@verticle)
    assert_response :success
  end

  test "should update verticle" do
    patch verticle_url(@verticle), params: { verticle: { name: @verticle.name } }
    assert_redirected_to verticle_url(@verticle)
  end

  # test "should destroy verticle" do
  #   assert_difference('Verticle.count', -1) do
  #     delete verticle_url(@verticle)
  #   end

  #   assert_redirected_to verticles_url
  # end
end
