require "application_system_test_case"

class VerticlesTest < ApplicationSystemTestCase
  setup do
    @verticle = verticles(:one)
  end

  test "visiting the index" do
    visit verticles_url
    assert_selector "h1", text: "Verticles"
  end

  test "creating a Verticle" do
    visit verticles_url
    click_on "New Verticle"

    fill_in "Name", with: @verticle.name
    click_on "Create Verticle"

    assert_text "Verticle was successfully created"
    click_on "Back"
  end

  test "updating a Verticle" do
    visit verticles_url
    click_on "Edit", match: :first

    fill_in "Name", with: @verticle.name
    click_on "Update Verticle"

    assert_text "Verticle was successfully updated"
    click_on "Back"
  end

  test "destroying a Verticle" do
    visit verticles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Verticle was successfully destroyed"
  end
end
